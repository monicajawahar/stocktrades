package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.StockList;

@SpringBootTest
public class StockListServiceTest {
	@Autowired
	StockListService repository;
		
	@Test
	public void testGetAllStockList() {
		List<StockList> returnedList = repository.getAll();
		
		assertThat(returnedList).isNotEmpty();
		/*for(StockList stock: returnedList) {
			System.out.println("Stock name: " + stock.getNameOfStock());
		}*/
	}
	
	@Test
	public void testGetStockListById() {
		StockList returnedList = repository.getById(1);
		assertThat(returnedList).isNotNull();
	}
	
	@Test
	public void testGetStockListByName() {
		StockList returnedList = repository.findByStockName("hdfc bank");
		assertThat(returnedList).isNotNull();
		
		//For failure test
		StockList returnedList1 = repository.findByStockName("axi bank");
		assertThat(returnedList1).isNull();
	}
	
	@Test
	public void testCUDStockList() {
		StockList stock = new StockList("SBI Bank","SBI", 1500);
		StockList returnedList = repository.addStock(stock);
		assertThat(returnedList).isNotNull();
		int id=returnedList.getId();
		
		StockList updateStock = new StockList(id,"SBI Bank","SBI", 1300);
		StockList returnedList1 = repository.updateStock(updateStock);
		assertThat(returnedList1).isNotNull();
	
		int responseId = repository.deleteStock(id);
		assertThat(responseId).isEqualTo(id);
	}
}
