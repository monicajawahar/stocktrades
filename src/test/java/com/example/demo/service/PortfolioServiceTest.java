package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Portfolio;

@SpringBootTest
public class PortfolioServiceTest {
	@Autowired
	PortfolioService repository;
		
	@Test
	public void testGetAllPortfolio() {
		List<Portfolio> returnedList = repository.getAll();
		
		assertThat(returnedList).isNotEmpty();
		/*for(Portfolio stock: returnedList) {
			System.out.println("Stock Ticker: " + stock.getStockTicker());
		}*/
	}
	
	@Test
	public void testGetPortfolioById() {
		Portfolio returnedList = repository.getById(1);
		assertThat(returnedList).isNotNull();
	}
	
	@Test
	public void testCUDPortfolio() {
		Portfolio stock = new Portfolio("nirmal","HDFC",3,100);
		Portfolio returnedList = repository.addStock(stock);
		assertThat(returnedList).isNotNull();
		int id=returnedList.getId();
		
		Portfolio updateStock = new Portfolio(id,"nirmal","AXIS",3,102);
		Portfolio returnedList1 = repository.updateStock(updateStock);
		assertThat(returnedList1).isNotNull();
	
		int responseId = repository.deleteStock(id);
		assertThat(responseId).isEqualTo(id);
	}
	
	@Test
	public void testGetStocksByUserName() {
		List<Portfolio> returnedList = repository.findByUserName("rithik");
		assertThat(returnedList).isNotEmpty();
		
		//for failure case
		List<Portfolio> returnedList1 = repository.findByUserName("xyz");
		assertThat(returnedList1).isEmpty();
	}
}
