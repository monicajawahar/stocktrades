package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.UserDetails;

@SpringBootTest
public class UserDetailsServiceTest {
	@Autowired
	UserDetailsService repository;
		
	@Test
	public void testGetAllUsers() {
		List<UserDetails> returnedList = repository.getAll();
		
		assertThat(returnedList).isNotEmpty();
		/*for(UserDetails user: returnedList) {
			System.out.println("Username: " + user.getUsername());
		}*/
	}
	
	@Test
	public void testGetUserById() {
		UserDetails returnedList = repository.getById(1);
		assertThat(returnedList).isNotNull();
	}
	
	@Test
	public void testGetUserByName() {
		UserDetails returnedList = repository.getUser("rithik");
		assertThat(returnedList).isNotNull();
	}
	
	@Test
	public void testCUDPortfolio() {
		UserDetails user = new UserDetails("john","dummypass","john@demo.com","9681765432","Joh Cole");
		UserDetails returnedList = repository.addUser(user);
		assertThat(returnedList).isNotNull();
		int id=returnedList.getId();
		
		UserDetails updateUser = new UserDetails(id,"john","hellopass","john@demo.com","9681765432","Joh Cole");
		UserDetails returnedList1 = repository.updateUser(updateUser);
		assertThat(returnedList1).isNotNull();
	
		int responseId = repository.deleteUser(id);
		assertThat(responseId).isEqualTo(id);
	}
	
	@Test
	public void testCheckUserPass() {
		String user = "rithik";
		String pass = "12345678";
		UserDetails response = repository.checkUserPass(user,pass);
		assertThat(response).isNotNull();
		
		//for failure case
		pass = "wrongpass";
		response = repository.checkUserPass(user,pass);
		assertThat(response).isNull();
	}
}
