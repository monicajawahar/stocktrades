package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.StockTrades;

@SpringBootTest
public class StockTradesServiceTest {
	@Autowired
	StockTradesService repository;
		
	@Test
	public void testGetAllStockTrades() {
		List<StockTrades> returnedList = repository.getAll();
		
		assertThat(returnedList).isNotEmpty();
		/*for(StockTrades stock: returnedList) {
			System.out.println("Stock name: " + stock.getNameOfStock());
		}*/
	}
	
	@Test
	public void testGetStockTradesById() {
		StockTrades returnedList = repository.getById(1);
		assertThat(returnedList).isNotNull();
	}
	
	@Test
	public void testGetStockTradesBetweenDates() {
		List<StockTrades> returnedList = repository.getStockBetweenDates("rithik","2021-08-21", "2021-08-28");
		assertThat(returnedList).isNotEmpty();
	}
	
	@Test
	public void testCUDStockTrades() {
		StockTrades stock = new StockTrades("HDFC",1250.0,200,"Buy",3,"HDFC Bank","rithik","2021-08-28 10:00:34",0);
		StockTrades returnedList = repository.addStock(stock);
		assertThat(returnedList).isNotNull();
		int id=returnedList.getId();
		
		StockTrades updateStock = new StockTrades(id,"HDFC",1200.0,200,"Buy",3,"HDFC Bank","rithik","2021-08-28 10:00:34",0);
		StockTrades returnedList1 = repository.updateStock(updateStock);
		assertThat(returnedList1).isNotNull();
	
		int responseId = repository.deleteStock(id);
		assertThat(responseId).isEqualTo(id);
	}
	
	@Test
	public void testGetStocksByUserName() {
		List<StockTrades> returnedList = repository.findByUserName("rithik");
		assertThat(returnedList).isNotEmpty();
		
		//for failure case
		List<StockTrades> returnedList1 = repository.findByUserName("xyz");
		assertThat(returnedList1).isEmpty();
	}
}
