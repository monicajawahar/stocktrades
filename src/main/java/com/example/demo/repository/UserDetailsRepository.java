package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.UserDetails;

@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, Integer>{
	@Query(
			value = "SELECT * FROM user_details WHERE lower(username) = lower(?1) and password = ?2", 
			nativeQuery = true)
	  UserDetails checkUserPass(String username,String password);
	
	@Query(
			value = "SELECT * FROM user_details WHERE lower(username) = lower(?1)", 
			nativeQuery = true)
	  UserDetails getUser(String username);
}
