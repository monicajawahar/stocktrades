package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.StockList;

@Repository
public interface StockListRepository extends JpaRepository<StockList, Integer>{
		@Query(
			value = "SELECT * FROM stock_list WHERE lower(name_of_stock) = lower(?1)", 
			nativeQuery = true)
	  StockList findByStockName(String nameOfStock);
}
