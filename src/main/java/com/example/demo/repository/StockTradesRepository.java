package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.StockTrades;

@Repository
public interface StockTradesRepository extends JpaRepository<StockTrades, Integer> {
	@Query(
			value = "SELECT * FROM stock_trades WHERE lower(username) = lower(?1)", 
			nativeQuery = true)
	  List<StockTrades> findByUserName(String username);

	/*@Query(
			value = "select * from stock_trades where date(date_time) between :fromDate and :toDate;", 
			//value = "select * from stock_trades;"  '" + name + "'", 
			nativeQuery = true)
	  List<StockTrades> findStockBetweenDates(@Param("fromDate") String fromDate, @Param("toDate") String toDate);*/

}
