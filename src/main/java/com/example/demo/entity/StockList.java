package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stock_list")
public class StockList {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String nameOfStock;
	
	private String stockTicker;

	private double price;

	public StockList() {
		
	}

	public StockList(int id, String nameOfStock, String stockTicker, double price) {
		super();
		this.id = id;
		this.nameOfStock = nameOfStock;
		this.stockTicker = stockTicker;
		this.price = price;
	}
	
	public StockList(String nameOfStock, String stockTicker, double price) {
		super();
		this.nameOfStock = nameOfStock;
		this.stockTicker = stockTicker;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameOfStock() {
		return nameOfStock;
	}

	public void setNameOfStock(String nameOfStock) {
		this.nameOfStock = nameOfStock;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	
}

