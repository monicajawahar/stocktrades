package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stock_trades")
public class StockTrades {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String stockTicker;
	
	private double price;
	
	private int volume;
	
	private String buyOrSell;
	
	private int inPortfolio;
	
	private int statusCode;
	
	private String nameOfStock;
	
	private String username;
	
	private String date_time;
	
	public StockTrades() {
		
	}
	
	
	public StockTrades(int id, String stockTicker, double price, int volume, String buyOrSell, int statusCode,
			String nameOfStock, String username, String date_time,int inPortfolio) {
		super();
		this.id = id;
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.nameOfStock = nameOfStock;
		this.username = username;
		this.date_time = date_time;
		this.inPortfolio = inPortfolio;
	}
	
	public StockTrades( String stockTicker, double price, int volume, String buyOrSell, int statusCode,
			String nameOfStock, String username, String date_time,int inPortfolio) {
		super();
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.nameOfStock = nameOfStock;
		this.username = username;
		this.date_time = date_time;
		this.inPortfolio = inPortfolio;
	}




	public int getInPortfolio() {
		return inPortfolio;
	}


	public void setInPortfolio(int inPortfolio) {
		this.inPortfolio = inPortfolio;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getNameOfStock() {
		return nameOfStock;
	}

	public void setNameOfStock(String nameOfStock) {
		this.nameOfStock = nameOfStock;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getDate_time() {
		return date_time;
	}


	public void setDate_time(String data_time) {
		this.date_time = data_time;
	}

	

}
