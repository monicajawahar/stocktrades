package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="portfolio")
public class Portfolio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String username;
	
	private String stockTicker;
	
	private int statusCode;
	
	private int volume;

	public Portfolio() {
		
	}

	public Portfolio(int id,String username, String stockTicker, int statusCode, int volume) {
		super();
		this.id = id;
		this.username = username;
		this.stockTicker = stockTicker;
		this.statusCode = statusCode;
		this.volume = volume;
	}
	
	public Portfolio(String username, String stockTicker, int statusCode, int volume) {
		super();
		this.username = username;
		this.stockTicker = stockTicker;
		this.statusCode = statusCode;
		this.volume = volume;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
}
