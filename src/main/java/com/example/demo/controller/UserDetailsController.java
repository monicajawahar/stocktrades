package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.UserDetails;
import com.example.demo.service.UserDetailsService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/user-details")
public class UserDetailsController {
	@Autowired
	UserDetailsService service;
	
	@GetMapping(value="/")
	public List<UserDetails> getAllUsers() {
		return service.getAll();
	}
	 
	@GetMapping(value = "/{id}")
	public UserDetails getUserById(@PathVariable("id") int id) {
	  return service.getById(id);
	}
	
	@GetMapping(value="/username={username}")
	public UserDetails getUser(@PathVariable("username") String username) {
		return service.getUser(username);
	}
	
	@GetMapping(value="/{username}/{password}")
	public UserDetails checkUser(@PathVariable("username") String username,@PathVariable("password") String password) {
		return service.checkUserPass(username, password);
	}

	@PostMapping(value = "/")
	public UserDetails addUser(@RequestBody UserDetails s) {
		return service.addUser(s);
	}

	@PutMapping(value = "/")
	public UserDetails editUser(@RequestBody UserDetails user) {
		return service.updateUser(user);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteUser(@PathVariable int id) {
		return service.deleteUser(id);
	}
}
