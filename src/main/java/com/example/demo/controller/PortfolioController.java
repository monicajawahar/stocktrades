package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Portfolio;
import com.example.demo.service.PortfolioService;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/portfolio")
public class PortfolioController {
	@Autowired
	PortfolioService service;
	
	@GetMapping(value="/")
	public List<Portfolio> getAllPortfolio() {
		return service.getAll();
	}
	 
	@GetMapping(value = "/{id}")
	public Portfolio getPortfolioById(@PathVariable("id") int id) {
	  return service.getById(id);
	}
	
	@GetMapping(value = "/username={username}")
	public List<Portfolio> getStockByUserName(@PathVariable("username") String nameOfUser) {
	  return service.findByUserName(nameOfUser);
	}

	@PostMapping(value = "/")
	public Portfolio addPortfolio(@RequestBody Portfolio s) {
		return service.addStock(s);
	}

	@PutMapping(value = "/")
	public Portfolio editPortfolio(@RequestBody Portfolio portfolio) {
		return service.updateStock(portfolio);
	}

	@DeleteMapping(value = "/{id}")
	public int deletePortfolio(@PathVariable int id) {
		return service.deleteStock(id);
	}
}
