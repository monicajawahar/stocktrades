package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.StockList;
import com.example.demo.service.StockListService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/stock-list")
public class StockListController {
	@Autowired
	StockListService service;
	
	@GetMapping(value="/")
	public List<StockList> getAllStocks() {
		return service.getAll();
	}
	 
	@GetMapping(value = "/{id}")
	public StockList getStockById(@PathVariable("id") int id) {
	  return service.getById(id);
	}
	
	@GetMapping(value = "/nameOfStock={nameOfStock}")
	public StockList getStockByName(@PathVariable("nameOfStock") String nameOfStock) {
	  return service.findByStockName(nameOfStock);
	}

	@PostMapping(value = "/")
	public StockList addStock(@RequestBody StockList s) {
		return service.addStock(s);
	}

	@PutMapping(value = "/")
	public StockList editStock(@RequestBody StockList stock) {
		return service.updateStock(stock);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStock(@PathVariable int id) {
		return service.deleteStock(id);
	}
}
