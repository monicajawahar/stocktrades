package com.example.demo.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.StockTrades;
import com.example.demo.service.StockTradesService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/stock-trades")
public class StockTradesController {
	
	@Autowired
	StockTradesService service;
	
	@GetMapping(value="/")
	public List<StockTrades> getAllStocks() {
		return service.getAll();
	}
	 
	@GetMapping(value = "/{id}")
	public StockTrades getStockById(@PathVariable("id") int id) {
	  return service.getById(id);
	}
	
	@GetMapping(value = "/username={username}")
	public List<StockTrades> getStockByUserName(@PathVariable("username") String nameOfUser) {
	  return service.findByUserName(nameOfUser);
	}

	@GetMapping(value = "/{username}/{fromDate}/{toDate}")
	public List<StockTrades> getStockBetweenDates(@PathVariable("username") String username,@PathVariable("fromDate") String fromDate,@PathVariable("toDate") String toDate) {
	  return service.getStockBetweenDates(username,fromDate,toDate);
	}
	
	@PostMapping(value = "/")
	public StockTrades addStock(@RequestBody StockTrades s) {
		return service.addStock(s);
	}

	@PutMapping(value = "/")
	public StockTrades editStockTrade(@RequestBody StockTrades stock) {
		return service.updateStock(stock);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStockTrades(@PathVariable int id) {
		return service.deleteStock(id);
	}
	

}
