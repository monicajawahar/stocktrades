package com.example.demo.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.example.demo.entity.StockTrades;
import com.example.demo.repository.StockTradesRepository;

@Service
public class StockTradesService {
	
	@Autowired
	StockTradesRepository repository;
	
	@Autowired
	private JdbcTemplate template;
	
	@PersistenceContext
	EntityManager em;
	
	public StockTrades addStock(StockTrades stock)
	{
		return repository.save(stock);
	}
	
	public StockTrades updateStock(StockTrades stock)
	{
		return repository.save(stock);
	}
	
	public int deleteStock(int id)
	{
		 repository.deleteById(id);
		 return id;
	}
	
	public StockTrades getById(int id)
	{
		return repository.findById(id).get();
	}
	
	public List<StockTrades> getAll()
	{
		return repository.findAll();
	}
	
	public List<StockTrades> findByUserName(String username)
	{
		return repository.findByUserName(username);
	}
	
	public List<StockTrades> getStockBetweenDates(String username,String fromDate, String toDate)
	{
		//return repository.findStockBetweenDates(fromDate,toDate);
		/*return em.createQuery(
		        "select s from stock_trades s where date(s.date_time) between :fromDate and :toDate")
				.setParameter("fromDate",fromDate)
				.setParameter("toDate",toDate)
				.getResultList();
		 */
		String sql = "select * from stock_trades where username='"+username+"' and date(date_time) between '"+fromDate+"' and '"+toDate+"'";
		return template.query(sql, new StockRowMapper());
	}
	
}

class StockRowMapper implements RowMapper<StockTrades> {

	@Override
	public StockTrades mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new StockTrades(rs.getInt("id"), rs.getString("stock_ticker"), rs.getDouble("price"), rs.getInt("volume"), rs.getString("buy_or_sell"),rs.getInt("status_code"),
				rs.getString("name_of_stock"), rs.getString("username"), rs.getString("date_time"),rs.getInt("in_portfolio"));
	}
}
