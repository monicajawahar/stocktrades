package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Portfolio;
import com.example.demo.repository.PortfolioRepository;

@Service
public class PortfolioService {
	@Autowired
	PortfolioRepository repository;
	
	public Portfolio addStock(Portfolio stock)
	{
		return repository.save(stock);
	}
	
	public Portfolio updateStock(Portfolio stock)
	{
		return repository.save(stock);
	}
	
	public int deleteStock(int id)
	{
		 repository.deleteById(id);
		 return id;
	}
	
	public Portfolio getById(int id)
	{
		return repository.findById(id).get();
	}
	
	public List<Portfolio> getAll()
	{
		return repository.findAll();
	}
	
	public List<Portfolio> findByUserName(String username)
	{
		return repository.findByUserName(username);
	}
}
