package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.UserDetails;
import com.example.demo.repository.UserDetailsRepository;

@Service
public class UserDetailsService {
	@Autowired
	UserDetailsRepository repository;
	
	public UserDetails addUser(UserDetails User)
	{
		return repository.save(User);
	}
	
	public UserDetails updateUser(UserDetails User)
	{
		return repository.save(User);
	}
	
	public int deleteUser(int id)
	{
		 repository.deleteById(id);
		 return id;
	}
	
	public UserDetails getById(int id)
	{
		return repository.findById(id).get();
	}
	
	public List<UserDetails> getAll()
	{
		return repository.findAll();
	}
	
	public UserDetails checkUserPass(String username,String password) {
		return repository.checkUserPass(username,password);
	}
	
	public UserDetails getUser(String username) {
		return repository.getUser(username);
	}
}
