package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.StockList;
import com.example.demo.repository.StockListRepository;

@Service
public class StockListService {
	@Autowired
	StockListRepository repository;
	
	public StockList addStock(StockList stock)
	{
		return repository.save(stock);
	}
	
	public StockList updateStock(StockList stock)
	{
		return repository.save(stock);
	}
	
	public int deleteStock(int id)
	{
		 repository.deleteById(id);
		 return id;
	}
	
	public StockList getById(int id)
	{
		return repository.findById(id).get();
	}
	
	public List<StockList> getAll()
	{
		return repository.findAll();
	}
	
	public StockList findByStockName(String nameOfStock)
	{
		return repository.findByStockName(nameOfStock);
	}
}
