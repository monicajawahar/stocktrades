create table IF NOT EXISTS stock_trades 
(id integer not null auto_increment, 
buy_or_sell varchar(255) not null, 
name_of_stock varchar(255) not null, 
price double precision not null, 
status_code integer not null, 
stock_ticker varchar(255), 
volume integer not null,
username varchar(255) not null,
date_time datetime not null,
in_portfolio integer not null,
primary key (id));

create table IF NOT EXISTS stock_list 
(id integer not null auto_increment, 
name_of_stock varchar(255) not null, 
price double precision not null, 
stock_ticker varchar(255) not null, 
primary key (id));

create table IF NOT EXISTS portfolio 
(id integer not null auto_increment, 
status_code integer not null, 
stock_ticker varchar(255) not null, 
username varchar(255) not null, 
volume integer not null, 
primary key (id));

create table IF NOT EXISTS user_details 
(id integer not null auto_increment, 
password varchar(255) not null, 
username varchar(255) not null,
email_id varchar(255) not null,
phone_no varchar(255) not null,
account_name varchar(255) not null,
primary key (id));
